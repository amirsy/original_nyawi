<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('password')
        ];
        \DB::table('users')->insert($user);
    }
}
