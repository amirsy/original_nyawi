<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('produk_id')->nullable()->constrained('products')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('transaksi_id')->nullable()->constrained('transaksis')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama');
            $table->string('no_telpon');
            $table->string('alamat', 500);
            $table->string('qty');
            $table->string('total_harga');
            $table->string('provinsi');
            $table->string('kabupaten');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksis');
    }
}
