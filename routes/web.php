<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Home Page */

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('about', 'HomeController@about')->name('about');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::get('infoproduct/{id}', 'ProductController@show')->name('infoproduct');
Route::get('kategori/{id}', 'HomeController@kategori')->name('kategori');

/* Link Whatsapp */
Route::post('whatsapp', 'LinkController@index')->name('whatsapp');
/* End Home Page */

/* Login Page & Authentikasi */
Route::get('administrator', 'LoginController@index')->name('login');
Route::post('login', 'LoginController@login')->name('postlogin');
Route::get('logout', 'LoginController@logout')->name('logout');
/* End Login Page & Authentikasi */

/* Administrator */
Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', 'HomeController@admin')->name('dashboard');

    /* Profile */
    Route::get('profile', 'ProfileController@index')->name('admin.profile');
    Route::get('profile/edit/{id}', 'ProfileController@edit')->name('profile.edit');
    Route::post('profile/update/{id}', 'ProfileController@update')->name('profile.update');

    /* Product */
    Route::get('product', 'ProductController@index')->name('admin.produk');
    Route::get('product/add', 'ProductController@create')->name('produk.add');
    Route::get('product/edit/{id}', 'ProductController@edit')->name('produk.edit');
    Route::post('product', 'ProductController@store')->name('produk.store');
    Route::post('product/update/{id}', 'ProductController@update')->name('produk.update');
    Route::get('product/{id}', 'ProductController@destroy')->name('produk.delete');

    /* Kategori */
    // Route::get('kategori', 'KategoriController@index')->name('admin.kategori');
    // Route::get('kategori/edit/{id}', 'KategoriController@edit')->name('kategori.edit');
    // Route::post('kategori', 'KategoriController@store')->name('kategori.store');
    // Route::post('kategori/update/{id}', 'KategoriController@update')->name('kategori.update');
    // Route::get('kategori/delete/{id}', 'KategoriController@destroy')->name('kategori.delete');
    
      /* Transaksi */
      Route::get('transaksi', 'TransaksiController@index')->name('admin.transaksi');
      Route::get('transaksi/add', 'TransaksiController@create')->name('transaksi.add');
      Route::get('transaksi/edit/{id}', 'TransaksiController@edit')->name('transaksi.edit');
      Route::post('transaksi', 'TransaksiController@store')->name('transaksi.store');
      Route::post('transaksi/update/{id}', 'TransaksiController@update')->name('transaksi.update');
      Route::get('transaksi/{id}', 'TransaksiController@destroy')->name('transaksi.delete');
      Route::get('transaksi/{transakasi}/invoice', 'TransaksiController@invoice')->name('transaksi.invoice');
      // Route::get('/transaksi/{transaksi}/transaksi_detail', 'TransaksiController@transaksi_detail')->name('transaksi.transaksi_detail');

       /* Transaksi */
       Route::get('customer', 'CustomerController@index')->name('admin.customer');
       Route::get('customer/add', 'CustomerController@create')->name('customer.add');
       Route::get('customer/edit/{id}', 'CustomerController@edit')->name('customer.edit');
       Route::post('customer', 'CustomerController@store')->name('customer.store');
       Route::post('customer/update/{id}', 'CustomerController@update')->name('customer.update');
       Route::get('customer/{id}', 'CustomerController@destroy')->name('customer.delete');
      // Route::get('customer/{transakasi}/invoice', 'CustomerController@invoice')->name('customer.invoice');
       // Route::get('/transaksi/{transaksi}/transaksi_detail', 'TransaksiController@transaksi_detail')->name('transaksi.transaksi_detail');

      /* Mahasiswa */
      Route::get('mahasiswa', 'MahasiswaController@index')->name('admin.mahasiswa');
      Route::get('mahasiswa/add', 'MahasiswaController@create')->name('mahasiswa.add');
      Route::get('mahasiswa/edit/{id}', 'MahasiswaController@edit')->name('mahasiswa.edit');
      Route::post('mahasiswa', 'MahasiswaController@store')->name('mahasiswa.store');
      Route::post('mahasiswa/update/{id}', 'MahasiswaController@update')->name('mahasiswa.update');
      Route::get('mahasiswa/{id}', 'MahasiswaController@destroy')->name('mahasiswa.delete');
      // Route::get('mahasiswa/{transakasi}/invoice', 'MahasiswaController@invoice')->name('mahasiswa.invoice');
      // Route::get('/mahasiswa/{mahasiswa}/mahasiswa_detail', 'MahasiswaController@mahasiswa_detail')->name('transaksi.transaksi_detail');

      /* kodereferal */
      Route::get('kodereferal', 'KodereferalController@index')->name('admin.kodereferal');
      Route::get('kodereferal/add', 'KodereferalController@create')->name('kodereferal.add');
      Route::get('kodereferal/edit/{id}', 'KodereferalController@edit')->name('kodereferal.edit');
      Route::post('kodereferal', 'KodereferalController@store')->name('kodereferal.store');
      Route::post('kodereferal/update/{id}', 'KodereferalController@update')->name('kodereferal.update');
      Route::get('kodereferal/{id}', 'KodereferalController@destroy')->name('kodereferal.delete');
      // Route::get('kodereferal/{transakasi}/invoice', 'KodereferalController@invoice')->name('kodereferal.invoice');
      // Route::get('/kodereferal/{kodereferal}/kodereferal_detail', 'KodereferalController@kodereferal_detail')->name('transaksi.transaksi_detail');

});
/* End Administrator */
