<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="{{ asset('../assets/img/favicon.ico') }}">
        <title>@yield('title')</title>

        <!-- Fonts and icons -->
        <link href="https://fonts.googleapis.com/css2?family=Lora&display=swap:400,500,600" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="../css/nucleo-icons.css" rel="stylesheet" />
        <link href="../css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="{{ asset('../css/nucleo-svg.css') }}" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="{{ asset('../css/soft-ui-dashboard.css?v=1.0.3') }}" rel="stylesheet" />
        <!-- Ck Editor -->
        <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
        <!-- Select2 -->
        <link href="{{asset('select2/css/select2.min.css')}}" rel="stylesheet" />
    </head>

    <body class="g-sidenav-show  bg-gray-100">
        @include('sweetalert::alert')

        @include('layout.component.sidebar')

        <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg">
            @yield('content')
        </main>
        <!--   Core JS Files   -->
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <script src="{{asset('../js/core/popper.min.js')}}"></script>
        <script src="{{asset('../js/core/bootstrap.min.js')}}"></script>
        <script src="{{asset('../js/plugins/perfect-scrollbar.min.js')}}"></script>
        <script src="{{asset('../js/plugins/smooth-scrollbar.min.js')}}"></script>
        <script src="{{asset('../js/soft-ui-dashboard.min.js?v=1.0.3')}}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="{{asset('select2/js/select2.min.js')}}"></script>

        @yield('script')
    </body>
</html>
