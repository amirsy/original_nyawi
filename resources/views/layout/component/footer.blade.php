<!-- Footer  -->
<footer>
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="footer-link">
                        <h4>Information</h4>
                        <a href="{{ route('about') }}">About Us</a>
                        <a href="{{ route('contact') }}">Contact Us</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="footer-link-contact">
                        <h4>Contact Us</h4>
                        <ul>
                            <li>
                                <p><i class="fas fa-map-marker-alt"></i>{!! $profile->alamat !!}</p>
                            </li>
                            <li>
                                <p><i class="fas fa-phone-square"></i>{{ $profile->no_tlp }}</p>
                            </li>
                            <li>
                                <p><i class="fas fa-envelope"></i>{{ $profile->email }}</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="footer-widget">
                        <h4>Social Media</h4>
                        <ul>
                            <li><a target="__blank" href="https://www.facebook.com/nyawijiclothing"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                            {{-- <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li> --}}
                            <li><a target="__blank" href="https://www.instagram.com/nyawiji.clothing/"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="https://wa.me/"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer  -->

<!-- copyright  -->
<div class="footer-copyright">
    <p class="footer-company">Copyright &copy;&nbsp;<script>
        document.write(new Date().getFullYear())
    </script>&nbsp;PT Inovasi Nuswantara Digital</p>
</div>
<!-- End copyright  -->