@extends('layout.admin_layouts')

@section('title', 'Administrator | Edit Produk')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('admin.produk') }}">Produk</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Edit Produk</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Edit Data Mahasiswa</h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    {{-- @php
        dd($produk->size);
    @endphp --}}
    <!-- Forms -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="container">
                            <form class="row g-3" method="POST" action="" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-12">
                                    <label for="formFile" class="form-label">User Name</label>
                                    <select name=""  id="" class="form-control">
                                        <option value="">----</option>
                                        <option value="">----</option>
                                        <option value="">----</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Nama Mahasiswa</label>
                                    <input class="form-control" required type="text" id="foto1" name="foto1">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Kode With Draw</label>
                                    <input class="form-control" type="text" id="foto2" name="foto2">
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('admin.mahasiswa') }}" type="button" class="btn btn-secondary float-start">Batal</a>
                                    <button type="submit" class="btn btn-primary float-end">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        CKEDITOR.replace( 'deskripsi' );
    </script>
    <script>
        $(document).ready(function() {
            $('.size').select2();
        });
        $(document).ready(function() {
            $('.color').select2();
        });
    </script>
@endsection
