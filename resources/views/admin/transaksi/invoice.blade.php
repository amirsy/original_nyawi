@extends('layout.admin_layouts')

@section('title', 'Administrator | Produk')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <h6 class="font-weight-bolder mb-0">Detail Transaksi</h6>
                <li class="nav-item d-xl-none mt-3 d-flex align-items-center">
                    <a href="#" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </a>
                </li>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 mt-3 d-flex align-items-center">
                    <div class="input-group">
                      
                        <form class="d-flex" action="" method="GET">
                            <input class="form-control me-2" placeholder="Cari ..." aria-label="Search" type="text" name="cari" value="{{ request('cari') }}">
                            <button class="input-group-text" type="submit"><i class="fas fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item mt-3 d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    {{-- <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="mb-0">Data Invoice</h6>
                            </div>
                        </div>
                    </div> --}}
                    <div class="card-body pb-0 p-3">
                        <div class="table-responsive p-0">
                            <section class="invoice">
                                <!-- title row -->
                                <div class="row">
                                  <div class="col-xs-12">
                                    <h2 class="page-header">
                                      <i class="fa fa-globe"></i> NYAWIJI.
                                      <small class="pull-right"> </small>
                                    </h2>
                                  </div>
                                  <!-- /.col -->
                                </div>
                                <!-- info row -->
                                <div class="row invoice-info">
                                  <div class="col-sm-4 invoice-col">
                                    From
                                    <address>
                                      <strong>PT Kala Citra Nuswantara</strong><br>
                                      Provinsi: <br>
                                      Kabupaten: <br>
                                      No Telpon: (804) 123-5432<br>
                                      Email: kalacitranuswantawa@gmail.com
                                    </address>
                                  </div>
                                  <!-- /.col -->
                                  <div class="col-sm-4 invoice-col">
                                    To
                                    <address>
                                      <strong>Nama Customer: </strong><br>
                                      Provinsi: <br>
                                      Kabupaten: <br>
                                      No Tepon: (555) 539-1037<br>
                                      Email: user@gmail.com
                                    </address>
                                  </div>
                                  <!-- /.col -->
                                  <div class="col-sm-4 invoice-col">
                                    <b>Kode Invoice: </b>
                                    <br><br>
                                    <b>Tanggal Transaksi:</b> 2/22/2014<br>
                                    <b>Kode LinkReferal:</b> 2/22/2014<br>
                                    <b>Jasa Kirim:</b> Raja Ongkir<br>
                                    <b>Status Pembayaran:</b> 2/22/2014<br>
                                    {{-- <b>Account:</b> 968-34567 --}}
                                  </div>
                                  <!-- /.col -->
                                </div>
                                <!-- /.row -->
                          
                                <!-- Table row -->
                                <div class="row">
                                  <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped">
                                      <thead>
                                      <tr>
                                        <th>Quantity</th>
                                        <th>Nama Produk</th>
                                        <th>Ukuran </th>
                                        <th>Warna</th>
                                        <th>Total Harga</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Call of Duty</td>
                                        <td>L</td>
                                        <td>Hitam</td>
                                        <td>Rp. 200.000</td>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>Need for Speed IV</td>
                                        <td>M</td>
                                        <td>Hitam</td>
                                        <td>Rp 100.000</td>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>Monsters DVD</td>
                                        <td>XL</td>
                                        <td>Merah</td>
                                        <td>Rp. 400.000</td>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>Grown Ups Blue Ray</td>
                                        <td>S</td>
                                        <td>Putih</td>
                                        <td>Rp. 200.000</td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!-- /.col -->
                                </div>
                                <!-- /.row -->
                          
                                <div class="row">
                                  <!-- accepted payments column -->
                                  <div class="col-xs-6">
                                    {{-- <p class="lead">Payment Methods:</p> --}}
                                    {{-- <img src="../../dist/img/credit/visa.png" alt="Visa">
                                    <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                                    <img src="../../dist/img/credit/american-express.png" alt="American Express">
                                    <img src="../../dist/img/credit/paypal2.png" alt="Paypal"> --}}
                          
                                    {{-- <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                      Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                                      dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                                    </p> --}}
                                  </div>
                                  <!-- /.col -->
                                  <div class="col-xs-5">
                                    
                          
                                    <div class="table-responsive" style="text-align: right">
                                      <table class="table">
                                        <tbody><tr>
                                          <th style="width:50%">Sub Total:</th>
                                          <td>Rp. 1000.000</td>
                                        </tr>
                                        <tr>
                                          <th>Ongkos Kirim:</th>
                                          <td>Rp. 30.000</td>
                                        </tr>
                                        <tr>
                                          <th>Total Harga:</th>
                                          <td>Rp. 1.200.000</td>
                                        </tr>
                                      </tbody></table>
                                    </div>
                                  </div>
                                  <!-- /.col -->
                                </div>
                                <!-- /.row -->
                          
                                <!-- this row will not appear when printing -->
                                <div class="row no-print">
                                  <div class="col-xs-12">
  
                                    <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Konfirmasi Pembayaran
                                    </button>
                                    <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                                      <i class="fa fa-download"></i> Print
                                    </button>
                                  </div>
                                </div>
                              </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.6.0.slim.js" integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>
    <script>
        $('.delete').click(function(){
            var produkid = $(this).attr('data-id');
            var produkname = $(this).attr('data-nama');
            swal({
                title: "Apakah anda yakin?",
                text: "Anda akan menghapus produk "+produkname+"",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "product/"+produkid+""
                    swal("Produk "+produkname+" berhasil dihapus", {
                    icon: "success",
                    });
                } else {
                    swal("Produk "+produkname+" batal dihapus");
                }
            });
        });
    </script>
@endsection
