@extends('layout.admin_layouts')

@section('title', 'Administrator | Produk')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Produk</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Transaksi</h6>
                <li class="nav-item d-xl-none mt-3 d-flex align-items-center">
                    <a href="#" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </a>
                </li>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 mt-3 d-flex align-items-center">
                    <div class="input-group">
                      
                        <form class="d-flex" action="" method="GET">
                            <input class="form-control me-2" placeholder="Cari ..." aria-label="Search" type="text" name="cari" value="{{ request('cari') }}">
                            <button class="input-group-text" type="submit"><i class="fas fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item mt-3 d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="mb-0">Data Transakasi</h6>
                            </div>
                            {{-- <div class="col-6 text-end">
                                <a class="btn bg-gradient-success text-dark mb-0" style="font-size: 12px" href="{{ route('produk.add') }}"><i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah</a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">No</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Nama Customer</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Kode Link Referal</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Tanggal Transaksi</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Status Pembayaran</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Detail Invoice</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Complete</span></td>
                                        <td>link referal</td>
                                        <td> 10/08/2020</td>
                                        <td> status</td>
                                        <td>
                                            <a class="btn bg-gradient-success text-dark mb-0" style="font-size: 12px" href="{{ route('transaksi.invoice', 1) }}"><i></i>&nbsp;&nbsp;INVOICE</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.6.0.slim.js" integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>
    <script>
        $('.delete').click(function(){
            var produkid = $(this).attr('data-id');
            var produkname = $(this).attr('data-nama');
            swal({
                title: "Apakah anda yakin?",
                text: "Anda akan menghapus produk "+produkname+"",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "product/"+produkid+""
                    swal("Produk "+produkname+" berhasil dihapus", {
                    icon: "success",
                    });
                } else {
                    swal("Produk "+produkname+" batal dihapus");
                }
            });
        });
    </script>
@endsection
