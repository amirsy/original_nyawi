@extends('layout.admin_layouts')

@section('title', 'Administrator | Edit Kategori')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('admin.kategori') }}">Kategori</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Edit Kategori</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Edit Kategori</h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Forms -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-8">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="container">
                            <form class="row g-3" method="POST" action="{{ route('kategori.update', $kategori->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-md-12 mb-5">
                                    <label for="kategori" class="form-label">Kategori Produk</label>
                                    <input type="text" class="form-control" id="kategori" name="kategori" placeholder="Kategori Produk" value="{{ $kategori->kategori }}">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto Kategori Produk</label>
                                    <input class="form-control" type="file" id="foto" name="foto">
                                </div>
                                <div class="col-12">
                                    <img src="{{ asset('uploads/kategori/'.$kategori->foto) }}" class="avatar avatar-sm me-3" alt="user1">
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('admin.kategori') }}" type="button" class="btn btn-secondary float-start">Batal</a>
                                    <button type="submit" class="btn btn-primary float-end">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
