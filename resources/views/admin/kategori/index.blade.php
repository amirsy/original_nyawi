@extends('layout.admin_layouts')

@section('title', 'Administrator | Kategori')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Kategori</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Kategori</h6>
                <li class="nav-item d-xl-none mt-3 d-flex align-items-center">
                    <a href="#" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </a>
                </li>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 mt-3 d-flex align-items-center">
                    <div class="input-group">
                        <form class="d-flex" action="{{ URL::current() }}" method="GET">
                            <input class="form-control me-2" placeholder="Cari ..." aria-label="Search" type="text" name="cari" value="{{ request('cari') }}">
                            <button class="input-group-text" type="submit"><i class="fas fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item mt-3 d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <div class="container-fluid py-4">
        <div class="col-12">
            <div class="row">
                <div class="col-xl-6 col-md-6 mb-xl-0 mb-4">
                    <div class="card">
                        <div class="position-relative">
                            <div class="card-header pb-0 p-3">
                                <div class="row">
                                    <div class="col-6 d-flex align-items-center">
                                        <h6 class="mb-0">Data Kategori</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pb-0 p-3">
                                <div class="table-responsive p-0">
                                    <table class="table align-items-center mb-0">
                                        <thead>
                                            <tr>
                                                <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">No</th>
                                                <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Kategori</th>
                                                <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Foto</th>
                                                <th class="text-secondary opacity-7"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $no = 1
                                            @endphp
                                                @forelse ($data as $item)
                                            <tr>
                                                <td>
                                                    <p class="text-xs font-weight-bold mb-0">{{ $no++ }}</p>
                                                </td>
                                                <td>
                                                    <p class="text-xs font-weight-bold mb-0">{{ $item->kategori }}</p>
                                                </td>
                                                <td>
                                                    <img src="{{ asset('uploads/kategori/'.$item->foto) }}" class="avatar avatar-sm me-3" alt="user1">
                                                </td>
                                                <td>
                                                    <a href="{{ route('kategori.edit', $item->id) }}" class="badge badge-sm bg-gradient-primary" data-toggle="tooltip" data-original-title="Edit">
                                                        <i class="fa fa-pencil me-sm-1"></i>
                                                    </a>
                                                    <a href="#" class="badge badge-sm bg-gradient-danger delete" data-id="{{ $item->id }}" data-nama="{{ $item->kategori }}" data-toggle="tooltip" data-original-title="Delete">
                                                        <i class="fa fa-trash me-sm-1"></i>
                                                    </a>
                                                </td>
                                                @empty
                                                <tr>
                                                    <td class="text-sm" colspan="15" align="center"><i>Data tidak tersedia</i></td>
                                                </tr>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                    {{ $data->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 mb-xl-0 mb-4">
                    <div class="card border-1">
                        <div class="position-relative">
                            <div class="card-header pb-0 p-3">
                                <div class="row">
                                    <div class="col-6 d-flex align-items-center">
                                        <h6 class="mb-0">Tambah Kategori</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pb-0 p-3">
                                <div class="table-responsive p-0">
                                    <form method="POST" action="{{ route('kategori.store') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="col-md-12 mb-3">
                                            <label for="kategori" class="form-label">Kategori Produk</label>
                                            <input type="text" class="form-control" id="kategori" name="kategori" placeholder="Kategori Produk">
                                        </div>
                                        <div class="col-12 mb-3">
                                            <label for="formFile" class="form-label">Upload Foto Kategori Produk</label>
                                            <input class="form-control" type="file" id="foto" name="foto">
                                        </div>
                                        <div class="col-12" style="text-align: center">
                                            <button type="submit" class="btn btn-primary col-8">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.6.0.slim.js" integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>
    <script>
        $('.delete').click(function(){
            var kategoriid = $(this).attr('data-id');
            var kategoriname = $(this).attr('data-nama');
            swal({
                title: "Apakah anda yakin?",
                text: "Anda akan menghapus kategori "+kategoriname+"",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "kategori/delete/"+kategoriid+""
                    swal("Kategori "+kategoriname+" berhasil dihapus", {
                    icon: "success",
                    });
                } else {
                    swal("Kategori "+kategoriname+" batal dihapus");
                }
            });
        });
    </script>
@endsection
