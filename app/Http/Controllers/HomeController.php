<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Product;
use App\Profile;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $product = Product::latest()->limit(12)->get();
        // dd($product);
        $profile = Profile::first();
        // $cat = Kategori::all();
        // dd($profile);

        return view('front.welcome', compact('product', 'profile'/* , 'cat' */));
    }

    public function profile()
    {
        $profile = Profile::first();
        return view('front.home.profile', compact('profile'));
    }

    public function about()
    {
        $profile = Profile::first();
        return view('front.home.about', compact('profile'));
    }

    public function contact()
    {
        $profile = Profile::first();
        return view('front.home.contact', compact('profile'));
    }

    public function admin()
    {
        $total = Product::all()->count();
        // $totalkat = Kategori::all()->count();
        // dd($total);
        $profile = Profile::first();
        return view('admin.dashboard', compact('total', 'profile'/* , 'totalkat' */));
    }

    // public function kategori($id)
    // {
    //     $profile = Profile::first();
    //     $product = Product::all();
    //     $kategori = Kategori::find($id);
    //     return view('front.home.kategori', compact('profile', 'product', 'kategori'));
    // }
}
