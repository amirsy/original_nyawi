<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LinkController extends Controller
{
    public function index(Request $request) {
        $request->validate([
            'nama' => "required",
            'ukuran' => "required",
            'warna' => "required"
        ]);

        // dd($request->all());
        $url = "https://wa.me/085781234321?text=Nama%20Produk%20%3A%20{$request->nama}%0AUkuran%20%3A%20{$request->ukuran}%0AWarna%20%3A%20{$request->warna}";
        // dd($url);
        return redirect()->away($url);
    }
}
