<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Product;
use App\Profile;
use App\Size;
use App\Warna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $produk = Product::with('size', 'warna')->paginate(5);
        $profile = Profile::first();
        // dd($produk);

        if ($request->cari) {
            if ($request->cari == "nama") {
                $produk = Product::where('nama')->latest()->paginate(5);
            } else {
                $produk = Product::where(function ($produk) use ($request) {
                    $produk->where('nama', 'like', "%$request->cari%");
                    $produk->orWhere('harga', 'like', "%$request->cari%");
                })->latest()->paginate(5);
            }
        }

        return view('admin.produk.index', compact('produk', 'profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = Profile::first();
        // $kategori = Kategori::all();
        return view('admin.produk.create', compact('profile',/*  'kategori' */));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        // dd($data);
        if($request->has('foto1')){
            $foto = $request->foto1;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto1'] = $new_foto;
        }
        if($request->has('foto2')){
            $foto = $request->foto2;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto2'] = $new_foto;
        }
        if($request->has('foto3')){
            $foto = $request->foto3;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto3'] = $new_foto;
        }
        if($request->has('foto4')){
            $foto = $request->foto4;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto4'] = $new_foto;
        }
        
        $produk = Product::create($data);

    
        foreach ($data['ukuran'] as $item) {
            $data2 = array(
                'product_id' => $produk->id,
                'ukuran' => $item
            );
            Size::create($data2);
        }

        foreach ($data['warna'] as $item) {
            $data3 = array(
                'product_id' => $produk->id,
                'warna' => $item
            );
            Warna::create($data3);
        }

        toast('Produk Berhasil Ditambahkan','success');
        return redirect('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::first();
        $produk = Product::with('size', 'warna')->find($id);
        // dd($produk);
        $data = Product::all();
        return view('front.home.detail', compact('produk', 'profile', 'data'));
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Product::with('size', 'warna')->findorfail($id);
        // dd($produk->size);
        // $kategori = Kategori::all();
        $profile = Profile::first();
        return view('admin.produk.edit', compact('profile', 'produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $produk = Product::findorfail($id);
        $data['foto1'] = $produk->foto1;
        $data['foto2'] = $produk->foto2;
        $data['foto3'] = $produk->foto3;
        $data['foto4'] = $produk->foto4;

        if($request->has('foto1')){
            $foto = $request->foto1;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto1'] = $new_foto;
        }
        if($request->has('foto2')){
            $foto = $request->foto2;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto2'] = $new_foto;
        }
        if($request->has('foto3')){
            $foto = $request->foto3;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto3'] = $new_foto;
        }
        if($request->has('foto4')){
            $foto = $request->foto4;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uploads/produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto4'] = $new_foto;
        }

        $produk->update($data);

        $size = Size::where('product_id', $produk->id)->delete();
        foreach ($data['ukuran'] as $item) {
            $data2 = array(
                'product_id' => $produk->id,
                'ukuran' => $item
            );
            Size::create($data2);
        }

        $size = Warna::where('product_id', $produk->id)->delete();
        foreach ($data['warna'] as $item) {
            $data3 = array(
                'product_id' => $produk->id,
                'warna' => $item
            );
            Warna::create($data3);
        }

        toast('Produk Berhasil Diupdate','success');
        return redirect('product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('products')->where('id', $id)->delete();

        toast('Produk Berhasil Dihapus','success');
        return redirect()->back();
    }
}
