<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        // 'kategori_id',
        'nama',
        'foto1',
        'foto2',
        'foto3',
        'deskripsi',
        'harga'
    ];

    // public function kategori()
    // {
    //     return $this->belongsTo(Kategori::class);
    // }

    public function size()
    {
        return $this->hasMany(Size::class, 'product_id', 'id');
    }
    public function warna()
    {
        return $this->hasMany(Warna::class, 'product_id', 'id');
    }
}
