<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warna extends Model
{
    protected $fillable = [
        'product_id',
        'warna',
    ];
}
